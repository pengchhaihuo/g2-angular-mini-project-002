import { Component, OnInit } from '@angular/core';
import {ArticleServiceService} from "../../../shared/service/article-service.service";
import {Observable} from "rxjs";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-view-page',
  templateUrl: './view-page.component.html',
  styleUrls: ['./view-page.component.css']
})
export class ViewPageComponent implements OnInit {

  id!: String;
  post!: any;

  constructor(private service:ArticleServiceService ,private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
    this.id = params.viewId;
    });
    this.service.getArticleById(this.id).subscribe(
      val=>{
        console.log(val.data);
        this.post = val.data;
      }
    );


  }

  ngOnDestroy() {

  }


}

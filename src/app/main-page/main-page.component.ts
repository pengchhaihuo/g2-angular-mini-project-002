import {Component, OnChanges, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {ArticleServiceService} from "../shared/service/article-service.service";
import {IArticle} from "../model/article";
import Swal from "sweetalert2";

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  constructor(private router: Router, private service: ArticleServiceService) {
  }

  posts!: IArticle[];
  isReadMore = true;

  ngOnInit(): void {
    this.getAll()
  }

  getAll() {
    this.service.getAllArticle().subscribe(
      val => {
        this.posts = val.data;
        // console.log(val.data)
      }
    )
  }

  onView(id: String) {
    this.router.navigateByUrl('view/' + id).then(r => {
    });
  }

  onEdit(id: String) {
    this.router.navigateByUrl('update/' + id).then(r => {
    });
  }

  onDelete(id: String, i:number) {
    this.service.deleteArticleById(id).subscribe(
      value => {
        // console.log(value.message)
        this.alertWithDanger(value.message);
      }
    );
    this.posts.splice(i,1);
  }

  showText() {
    this.isReadMore = !this.isReadMore
  }

  alertWithDanger(message:any){
    Swal.fire('', message, 'warning')
  }
}
